# pyrest simple python structured rest api using flask-restplus
# Copyright (C) 2020 Bruno Mondelo

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint
from flask_restplus import Api

from app_v1.routes import setRoutes

api_v1 = Blueprint('api', __name__, url_prefix='/api/v1')

api = Api(
    api_v1,
    title='PyRest API',
    version='1.0',
    description='PyRest simple API',
)

setRoutes(api)
